package ru.kpfu.itis.group907.Khasanova.task3;

import javafx.util.Pair;

import java.io.*;
import java.util.*;

public class SaleStrategyResearch {
    public static void main(String[] args) throws FileNotFoundException {
        String csvFile = "transactions.csv";
        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        readProduct(br);

        Pair<String, String> product = readProduct(br);
        HashMap<String, Integer> salesCounts = new HashMap<>();
        HashMap<String, List<String>> bucketsMap = new HashMap<>();
        while (product != null) {
            String productId = product.getValue();
            Integer salesCount = salesCounts.getOrDefault(productId, 0);
            salesCounts.put(productId, salesCount + 1);

            String bucketId = product.getKey();
            bucketsMap.putIfAbsent(bucketId, new ArrayList<>());
            List<String> productsInBucket = bucketsMap.get(bucketId);
            productsInBucket.add(productId);

            product = readProduct(br);
        }

        List<Map.Entry<String, Integer>> sortedProductsCounts = new ArrayList<>(salesCounts.entrySet());
        sortedProductsCounts.sort(
                Comparator
                        .<Map.Entry<String, Integer>, Integer>comparing(p -> p.getValue())
                        .reversed());
        List<String> sortedProducts = new ArrayList<>();
        for (Map.Entry<String, Integer> productEntry : sortedProductsCounts)
            sortedProducts.add(productEntry.getKey());

        Map<String, Map<String, Integer>> complementProductsMap = new HashMap<>();
        for (String bucketId : bucketsMap.keySet()) {
            List<String> bucket = bucketsMap.get(bucketId);
            for (String productId : bucket) {
                complementProductsMap.putIfAbsent(productId, new HashMap<>());
                Map<String, Integer> complementProducts = complementProductsMap.get(productId);

                for (String productToAddId : bucket) {
                    if (productId.equals(productToAddId))
                        continue;

                    Integer productQuantity = complementProducts.getOrDefault(productToAddId, 0);
                    complementProducts.put(productToAddId, productQuantity + 1);
                }
            }
        }

        saveToCsvFile(complementProductsMap, sortedProducts, "complement_products.csv");

        System.out.println(complementProductsMap);
    }

    private static Pair<String, String> readProduct(BufferedReader reader) {
        String rawLine;
        try {
            rawLine = reader.readLine();
            if (rawLine == null)
                return null;
        } catch (IOException e) {
            return null;
        }

        String[] items = rawLine.split(";");
        return new Pair<>(items[1], items[0]);
    }

    private static void saveToCsvFile(
            Map<String, Map<String, Integer>> complementProductsMap,
            List<String> sortedProducts,
            String fileName) {
        try (PrintWriter file = new PrintWriter(new FileWriter(fileName))) {
            for (String productId : sortedProducts) {
                Map<String, Integer> complementProducts = complementProductsMap.getOrDefault(productId, null);
                if (complementProducts == null)
                    continue;

                List<Map.Entry<String, Integer>> sortedComplementProductsCounts = new ArrayList<>(complementProducts.entrySet());
                sortedComplementProductsCounts.sort(
                        Comparator
                                .<Map.Entry<String, Integer>, Integer>comparing(p -> p.getValue())
                                .reversed());
                List<String> sortedComplementProducts = new ArrayList<>();
                for (Map.Entry<String, Integer> productEntry : sortedComplementProductsCounts)
                    sortedComplementProducts.add(productEntry.getKey());

                for (String complementProductId : sortedComplementProducts) {
                    Integer complementProductFrequency = complementProducts.get(complementProductId);
                    file.println(productId + ";" + complementProductId + ";" + complementProductFrequency);
                }
            }
        } catch (IOException e) {
            return;
        }
    }
}
