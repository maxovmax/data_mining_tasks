package ru.kpfu.itis.group907.Khasanova.task2;

import javafx.util.Pair;

import java.io.*;
import java.util.*;

public class ProductsWithPurchasingPower {
    public static void main(String[] args) throws FileNotFoundException {
        String csvFile = "transactions.csv";
        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        readProduct(br);

        Pair<String, String> product = readProduct(br);
        HashMap<String, Integer> salesCounts = new HashMap<>();
        while (product != null) {
            String productId = product.getValue();
            Integer salesCount = salesCounts.getOrDefault(productId, 0);
            salesCounts.put(productId, salesCount + 1);

            product = readProduct(br);
        }

        List<Map.Entry<String, Integer>> sortedProducts = new ArrayList<>(salesCounts.entrySet());
        sortedProducts.sort(
                Comparator
                        .<Map.Entry<String, Integer>, Integer>comparing(p -> p.getValue())
                        .reversed());

        saveToCsvFile(sortedProducts, "products.csv");

        System.out.println(sortedProducts);
    }

    private static Pair<String, String> readProduct(BufferedReader reader) {
        String rawLine;
        try {
            rawLine = reader.readLine();
            if (rawLine == null)
                return null;
        } catch (IOException e) {
            return null;
        }

        String[] items = rawLine.split(";");
        return new Pair<>(items[1], items[0]);
    }

    private static void saveToCsvFile(List<Map.Entry<String, Integer>> products, String fileName) {
        try (PrintWriter file = new PrintWriter(new FileWriter(fileName))) {
            for (Map.Entry<String, Integer> product : products) {
                file.println(product.getKey() + ";" + product.getValue());
            }
        } catch (IOException e) {
            return;
        }
    }
}
